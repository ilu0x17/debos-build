# debos-build

This repository contains the recipes required to build an image for Kunbus
GmbH's RevolutionPi line.

## Building

Install the dependencies:

- [debos](https://packages.debian.org/stable/debos)


Using debos is quite easy. Building the "lite" RevolutionPi image:

```sh
make lite
```

The Makefile supports the following variables:

- `ARCH`: The architecture to build for. Default: `arm64`
- `SUITE`: The Debian release to build. Default: `bookworm`
- `IMAGE_SIZE`: The size of the output image. Default: `1.5 GB`
- `LOCAL_DEBS`: Whether to install Debian packages from the local `debs-to-install`
  directory. Default: `false`.
- `DO_IMAGE`: Produce an image as the output. Default: `true`
- `DO_TAR`: Produce a tarball as the output. Default: `false`
- `DO_COMPRESS`: Compress the output image. Default: `true`
- `DATE`: The value to use as the date field. Default: Output of `date
  +%Y-%m-%d`
- `VERSION`: The version the image is given. This is only used when a snapshot
  is built. Default: Output of `git describe ...`
- `DEBOS_FLAGS`: Additional flags to pass to debos. Overrides any flags used in
  the Makefile. Default: empty
- `RELEASE`: Build a release image. Default: `false`

For example, to create an `arm64` release image for `bullseye` as well as a tarball:

```sh
make ARCH=arm64 RELEASE=1 SUITE=bullseye DO_TAR=true
```
