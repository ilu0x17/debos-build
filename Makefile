DEBOS ?= debos

VERSION ?= $(shell git describe --long --abbrev=12 --tags --dirty --always 2>/dev/null)
DATE ?= $(shell date +%Y-%m-%d)

ARCH ?= arm64
SUITE ?= bookworm
IMAGE_SIZE ?= 1.5 GB
LOCAL_DEBS ?= false
DO_IMAGE ?= true
DO_TAR ?= false
DO_COMPRESS ?= true

ifdef RELEASE
	_DATE = $(DATE)
else
	_DATE = $(VERSION)_$(DATE)
endif

_DEBOS_FLAGS ?= \
	-tarch:"$(ARCH)" \
	-tsuite:"$(SUITE)" \
	-timage_size:"$(IMAGE_SIZE)" \
	-tlocal_debs:"$(LOCAL_DEBS)" \
	-tdo_image:"$(DO_IMAGE)" \
	-tdo_tar:"$(DO_TAR)" \
	-tdo_compress:"$(DO_COMPRESS)"
__DEBOS_FLAGS = $(_DEBOS_FLAGS) $(DEBOS_FLAGS)

default: lite

# main build recipe
%: revpi-template.yaml templates/%.yaml
	$(DEBOS) \
		-timage_type:"$@" \
		-timage:"$(_DATE)-revpi-$(SUITE)-$(ARCH)-$@.img" \
		$(__DEBOS_FLAGS) \
		$<

.PHONY: clean
clean:
	rm -rf *.img *.img.xz *.img.md5sum.txt *.tar.gz .debos-*
